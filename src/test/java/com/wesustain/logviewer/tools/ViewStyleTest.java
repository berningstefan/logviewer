/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author sberning
 */

public class ViewStyleTest {

	@Test
	public void testGetter() {
		ViewStyle viewStyle = new ViewStyle(1, "Name", "Type", "Expr", "bgColor", "fgColor", 0, 0);
		assertAll(
				() -> assertEquals(1, viewStyle.getStyleID()),
				() -> assertEquals("Name", viewStyle.getName()),
				() -> assertEquals("Type", viewStyle.getType()),
				() -> assertEquals("Expr", viewStyle.getExpression()),
				() -> assertEquals("bgColor", viewStyle.getBgColor()),
				() -> assertEquals("fgColor", viewStyle.getFgColor()),
				() -> assertEquals(0, viewStyle.getNotification()),
				() -> assertEquals(0, viewStyle.getMail())
		);
	}


	@Test
	public void testSetter() {
		ViewStyle viewStyle = new ViewStyle(0, "", "", "", "", "", 0, 0);
		viewStyle.setStyleID(1);
		viewStyle.setName("Name");
		viewStyle.setType("Type");
		viewStyle.setExpression("Expr");
		viewStyle.setBgColor("bgColor");
		viewStyle.setFgColor("fgColor");
		viewStyle.setNotification(1);
		viewStyle.setMail(1);


		assertAll(
				() -> assertEquals(1, viewStyle.getStyleID()),
				() -> assertEquals("Name", viewStyle.getName()),
				() -> assertEquals("Type", viewStyle.getType()),
				() -> assertEquals("Expr", viewStyle.getExpression()),
				() -> assertEquals("bgColor", viewStyle.getBgColor()),
				() -> assertEquals("fgColor", viewStyle.getFgColor()),
				() -> assertEquals(1, viewStyle.getNotification()),
				() -> assertEquals(1, viewStyle.getMail())
		);
	}
}
