/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.wesustain.logviewer.LogViewerApplication;

import lombok.extern.slf4j.Slf4j;
import static org.junit.jupiter.api.Assertions.*;


/**
 * @author sberning
 */
@Slf4j
@ExtendWith (SpringExtension.class)
@SpringBootTest (classes = LogViewerApplication.class)
public class DatabaseManagerTest {

	@Autowired
	private DatabaseManager dbm;


	@Test
	public void getUser() throws ParseException {
		LoginAuthenticationProvider.User user = dbm.getUser("admin");
		assertNull(user);
		String expectedStr = dbm.addUser((JSONObject)new JSONParser().parse("{\"username\":\"admin\",\"password\":\"password\",\"userrole\":\"ROLE_ADMIN\"}"));
		assertEquals("", expectedStr);
		assertNotNull(dbm.getUser("admin"));
	}


	@Test
	public void saveNewCSS() throws ParseException {
		String expectedStr = dbm.saveNewCSS(
				(JSONObject)new JSONParser().parse("{\"NoValidField\":\"NoValue\"}"));
		assertEquals("Error saving style to database!", expectedStr);

		expectedStr = dbm.saveNewCSS(
				(JSONObject)new JSONParser().parse("{\"name\":\"StyleName\",\"type\":\"Type\",\"expression\":\"expr\",\"fgcolor\":\"white\","
						+ "\"bgcolor\":\"black\",\"notify\":0,\"mail\":0}"));
		assertEquals("", expectedStr);

	}


	@Test
	public void loadViewStyle() throws ParseException {
		ViewStyle vs = dbm.loadViewStyle(1);
		assertNull(vs);

		String res = dbm.saveNewCSS(
				(JSONObject)new JSONParser().parse("{\"name\":\"StyleName\",\"type\":\"Type\",\"expression\":\"expr\",\"fgcolor\":\"white\","
						+ "\"bgcolor\":\"black\",\"notify\":0,\"mail\":0}"));

		vs = dbm.loadViewStyle(1);
		assertEquals("StyleName", vs.getName());

	}


	@Test
	public void loadAllStyles() throws ParseException {
		List<ViewStyle> viewStyles = dbm.loadAllStyles();
		assertEquals(0, viewStyles.size());

		String res = dbm.saveNewCSS(
				(JSONObject)new JSONParser().parse("{\"name\":\"StyleName\",\"type\":\"Type\",\"expression\":\"expr\",\"fgcolor\":\"white\","
						+ "\"bgcolor\":\"black\",\"notify\":0,\"mail\":0}"));

		viewStyles = dbm.loadAllStyles();
		assertEquals(1, viewStyles.size());
	}


	@Test
	public void deleteStylesInRange() throws ParseException {
		String expectedStr = dbm.deleteStylesInRange(
				(JSONObject)new JSONParser().parse("{\"NoValidKey\":\"1\"}")
		);
		assertEquals("Error deleting styles from database!", expectedStr);


		String res = dbm.saveNewCSS(
				(JSONObject)new JSONParser().parse("{\"name\":\"StyleName\",\"type\":\"Type\",\"expression\":\"expr\",\"fgcolor\":\"white\","
						+ "\"bgcolor\":\"black\",\"notify\":0,\"mail\":0}"));


		expectedStr = dbm.deleteStylesInRange(
				(JSONObject)new JSONParser().parse("{\"idlist\":\"1\"}")
		);
		assertEquals("", expectedStr);

	}


	@Test
	public void addUser() throws ParseException {
		String expectedStr = dbm.addUser((JSONObject)new JSONParser().parse("{\"username\":\"admin\",\"password\":\"password\",\"userrole\":\"ROLE_ADMIN\"}"));
		assertEquals("", expectedStr);
		expectedStr = dbm.addUser((JSONObject)new JSONParser().parse("{\"username\":\"admin\",\"password\":\"password\",\"userrole\":\"ROLE_ADMIN\"}"));
		assertEquals("This name is in use!", expectedStr);
		expectedStr = dbm.addUser((JSONObject)new JSONParser().parse("{\"NoValidKey\":\"admin\",\"password\":\"password\",\"userrole\":\"ROLE_ADMIN\"}"));
		assertEquals("Error adding user into database!", expectedStr);

	}


	@Test
	public void checkDatabaseIOErrors() throws IOException {
		File file = new File(System.getProperty("user.dir") + "/LogViewer.db");
		file.createNewFile();
		file.setWritable(false);
		file.setReadable(false);
		assertEquals(0, dbm.loadAllStyles().size());
	}


	@AfterEach
	private void deleteDB() {
		File file = new File(System.getProperty("user.dir") + "/LogViewer.db");
		file.setReadable(true);
		file.setWritable(true);

		if (!file.delete()) {
			log.warn("Could not delete database");
		}
	}

}
