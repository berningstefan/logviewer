/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.config;

import java.util.Properties;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author sberning
 */

public class PreferencesTest {

	@BeforeEach
	private void setResDir() {
		final String SEP = System.getProperty("file.separator");
		System.setProperty("resources.dir", System.getProperty("user.dir") + SEP + "src" + SEP + "test" + SEP + "resources" + SEP);
	}


	@Test
	public void getPreferencesException() {
		System.setProperty("resources.dir", "C:\\to\\nowhere\\");
		assertThrows(RuntimeException.class, () -> new Preferences());
	}


	@Test
	public void getPreference() {
		System.setProperty("resources.dir", "src\\test\\resources\\");
		Preferences preferences = new Preferences();
		assertEquals("5000", Preferences.getPreference("readlog.interval"));
		assertEquals("", Preferences.getPreference("NoKey"));
		assertThrows(NullPointerException.class, () -> Preferences.getPreference(null));
	}


	@Test
	public void getMailPreferences() {
		System.setProperty("resources.dir", "src\\test\\resources\\");
		Preferences preferences = new Preferences();
		Properties props = Preferences.getMailPreferences();
		assertEquals("true", props.getProperty("mail.smtp.auth"));
		assertEquals("false", props.getProperty("mail.smtp.starttls.enable"));
		assertEquals("*", props.getProperty("mail.smtp.ssl.trust"));
		assertEquals("localhost", props.getProperty("mail.smtp.host"));
		assertEquals("25", props.getProperty("mail.smtp.port"));
	}
}