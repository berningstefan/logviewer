var clusterize;
var contentNew = false;

var noop = setInterval(
	function() {
		var request = new XMLHttpRequest();
		request.open("GET", document.location + "noop");
		request.addEventListener('load', function(event) {
			if (request.status >= 200 && request.status < 300) {
				console.log("NOOP-Request: " + request.responseText);
			}
			else {
				console.warn("NOOP-Request warn: " + request.statusText);
			}
		});
		request.send();

		var jsontext = "{\"action\":\"noop\",\"data\":[{\"\":\"\"}]}";
		send(jsontext);


	}, 15 * 60000); // 15Min

document.addEventListener('DOMContentLoaded', function() {
	var data = "";
	clusterize = new Clusterize({
		rows : data,
		scrollId : 'scrollArea',
		contentId : 'contentArea'
	});
});

function showAlert(message) {
	var alertBox = document.getElementById('alert');
	alertBox.innerHTML = "<p class=\"alertmessage\">" + message + "</p><p class=\"btn\" onclick=\"javascript:closeAlert();\">OK</p>";
	alertBox.style.left = (window.innerWidth / 2) - 150 + "px";
	alertBox.style.visibility = "visible";
}

function closeAlert() {
	document.getElementById('alert').style.visibility = "hidden";
}


function showMenu() {
	document.getElementById('menu').style.visibility = "visible";
}

function hideMenu() {
	document.getElementById('menu').style.visibility = "hidden";
}


function showStylesEditBox() {
	var stylesEditBox = document.getElementById('styleseditbox');
	stylesEditBox.style.left = (window.innerWidth / 2) - 220 + "px";
	stylesEditBox.style.visibility = "visible";
}

function closeStylesEditBox() {
	document.getElementById('styleseditbox').style.visibility = "hidden";
}


function showAllStylesListBox() {
	var allStylesListBox = document.getElementById('allstyleslistbox');

	var jsontext = '{"action" : "getallstyles", "data" : [{"":""}]}';
	send(jsontext);

	allStylesListBox.style.left = (window.innerWidth / 2) - 250 + "px";
	allStylesListBox.style.visibility = "visible";
}

function hideAllStylesListBox() {
	document.getElementById("allStylesContent").innerHTML = "";
	document.getElementById('allstyleslistbox').style.visibility = "hidden";
}

function showAdduserBox() {
	var addUserBox = document.getElementById("adduserbox");
	addUserBox.style.left = (window.innerWidth / 2) - 170 + "px";
	addUserBox.style.visibility = "visible";
}

function closeAdduserBox() {
	document.getElementById('adduserbox').style.visibility = "hidden";
}

function saveNewUser() {
	var err = "";
	var username = document.getElementById("usernamefld").value;
	var password = document.getElementById("userpassfld").value;
	var userrole = document.getElementById("userrolelist").value;
	if (username.length === 0 || password.length === 0) {
		err += "Name and password must be set!<br>";
	}
	if (password.length !== 0 && password.length < 8) {
		err += "Password to short! (min. 8 chars.)";
	}

	if (err.length > 0) {
		showAlert(err);
	}
	else {
		var jsontext = "{\"action\":\"adduser\",\"data\":[{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"userrole\":\"" + userrole + "\"}]}";
		send(jsontext);
	}
}

function startLogReader() {
	var logFileName = document.getElementById('fileselect').value;
	var lineStyleID = document.getElementById('linestyleselect').value;
	var sectStyleID = document.getElementById('sectstyleselect').value;
	if (logFileName.length !== 0) {
		clusterize.update(["Loading data..."]);
		contentNew = true;
		var jsontext = '{"action" : "startlogreader", "data" : [{"logFileName":"' + logFileName + '", "lineStyleID":"' + lineStyleID + '", "sectStyleID":"' + sectStyleID + '"}]}';
		send(jsontext);
	}
	else {
		showAlert("No logfile selected!");
	}
}

function saveNewCSS() {
	var err = "";
	var type = document.getElementById('typeslct').value;
	var name = document.getElementById('namefld').value;
	var expression = document.getElementById('expressionfld').value;
	var bgcolor = document.getElementById('bgcolorfld').value;
	var fgcolor = document.getElementById('fgcolorfld').value;
	var notify = document.getElementById('notifybx').checked;
	var mail = document.getElementById('mailbx').checked;

	var regex = new RegExp('^-?[_a-zA-Z]+[_a-zA-Z0-9-]*$', 'gi'); // only this is allowed in css classnames
	if (!regex.test(name)) {
		err += "The given name is not valid!<br>";
	}
	if (name !== "" && expression !== "" && bgcolor !== "" && fgcolor !== "") {
		notify = notify === true ? 1 : 0;
		mail = mail === true ? 1 : 0;
		var jsontext = '{"action" : "savenewcss", "data" : [{';
		jsontext += '"type" : "' + type + '", ';
		jsontext += '"name" : "' + name + '", ';
		jsontext += '"expression" : "' + expression + '", ';
		jsontext += '"bgcolor" : "' + bgcolor + '", ';
		jsontext += '"fgcolor" : "' + fgcolor + '", ';
		jsontext += '"notify" : ' + notify + ', ';
		jsontext += '"mail" : ' + mail + '}]}';
	}
	else {
		err += 'Please fill all fields!';
	}

	if (err === "") {
		send(jsontext);
	}
	else {
		showAlert(err);
	}
}


function saveDeleteStyles() {
	var checkboxes = document.getElementById('allStylesContent').getElementsByTagName('input');
	var data = "";
	for (var i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i].checked) {
			if (data !== "") {
				data += ",";
			}
			data += checkboxes[i].value;
		}

	}
	if (data === "") {
		showAlert("No style to delete selected!")
	}
	else {
		var jsontext = "{\"action\":\"savedeletestyles\",\"data\":[{\"idlist\":\"" + data + "\"}]}";
		send(jsontext);
	}
}

function updateFgcolorField(picker) {
	document.getElementById('fgcolorfld').value = picker.value;
}

function updateBgcolorField(picker) {
	document.getElementById('bgcolorfld').value = picker.value;
}

function logout() {
	socket.close();
	location.href = location.href + "logout";
}

function search(evt) {
	if (evt.keyCode == 13) {
		doSearch();
	}
}

function doSearch() {
	var searchVal = document.getElementById('searchfield').value;
	if (searchVal === "") {
		return;
	}

	contentNew = true;
	clusterize.update(["Searching..."]);
	var jsontext = "{\"action\":\"dosearch\",\"data\":[{\"lookfor\":\"" + searchVal + "\"}]}";
	send(jsontext);
}