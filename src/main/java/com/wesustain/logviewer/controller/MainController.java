/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.controller;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import com.wesustain.logviewer.config.Preferences;
import com.wesustain.logviewer.tools.DatabaseManager;
import com.wesustain.logviewer.tools.ViewStyle;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
@Controller
public class MainController {

	@Autowired
	private DatabaseManager dbm;


	@NonNull
	@RequestMapping ("/noop")
	@ResponseBody
	public String noop() {
		return "NOOP ok";
	}


	@NonNull
	@RequestMapping ("/")
	public String index(@NonNull Model model) {
		StringBuilder logFilesOpts = new StringBuilder();
		StringBuilder lineStylesOpts = new StringBuilder();
		StringBuilder sectStylesOpts = new StringBuilder();
		StringBuilder stylesheet = new StringBuilder();
		StringBuilder menuopts = new StringBuilder();

		// list the logfiles
		String logfilePath = Preferences.getPreference("logfiles.path");
		logFilesOpts.append("\n<option value=\"\">Log-File</option>\n");
		if (!logfilePath.isEmpty()) {
			File[] logFiles = new File(logfilePath).listFiles();
			if (logFiles != null) {
				Arrays.sort(logFiles);
				for (File file : logFiles) {
					if (file.isFile() && !file.getName().endsWith(".gz") && file.canRead()) {
						String emptyFile = "";
						if (file.length() == 0) {
							emptyFile = " (Empty File)";
						}
						logFilesOpts.append("<option value=\"");
						logFilesOpts.append(HtmlUtils.htmlEscape(file.getName()));
						logFilesOpts.append("\">");
						logFilesOpts.append(HtmlUtils.htmlEscape(file.getName())).append(emptyFile);
						logFilesOpts.append("</option>\n");
					}
				}
			}
		}
		else {
			log.error("Path to logfiles not set!\n");
		}


		// list the styles
		lineStylesOpts.append("\n<option value=\"\">Line Style</option>\n");
		sectStylesOpts.append("\n<option value=\"\">Section Style</option>\n");

		List<ViewStyle> viewStyles = dbm.loadAllStyles();
		for (ViewStyle v : viewStyles) {
			StringBuilder sb = v.getType().equals("line") ? lineStylesOpts : sectStylesOpts;

			sb.append("<option value=\"");
			sb.append(v.getStyleID());
			sb.append("\">");
			sb.append(HtmlUtils.htmlEscape(v.getName()));
			sb.append("</option>\n");

			stylesheet.append(v.getType().equals("line") ? "#contentArea p." : "#contentArea p span.");
			stylesheet.append(v.getName()).append("{ color: ").append(v.getFgColor()).append("; background-color: ").append(v.getBgColor()).append("; "
					+ "}\n");
		}

		// Menu options
		menuopts.append("<li onclick=\"showStylesEditBox();\">New Style...</li>");
		menuopts.append("<li class=\"separator\" onclick=\"showAllStylesListBox();\">Delete Style...</li>");
		menuopts.append("<li class=\"separator\" onclick=\"showAdduserBox();\">Add User...</li>");
		menuopts.append("<li onclick=\"logout();\">Logout...</li>");


		model.addAttribute("logfileopts", logFilesOpts.toString());
		model.addAttribute("linestyleopts", lineStylesOpts.toString());
		model.addAttribute("sectstyleopts", sectStylesOpts.toString());
		model.addAttribute("stylesheet", stylesheet.toString());
		model.addAttribute("websocket", "var socket = new WebSocket(\"" + Preferences.getPreference("websocket.url") + "\");");
		model.addAttribute("year", Calendar.getInstance().get(Calendar.YEAR));
		model.addAttribute("menuopts", menuopts);

		return "index";
	}

}
