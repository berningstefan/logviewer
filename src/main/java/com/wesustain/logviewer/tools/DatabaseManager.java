/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
@Component
public class DatabaseManager {

	@Autowired
	private JdbcTemplate jdbcTemplate;


	private void checkDatabase() {
		boolean stylesheetsOK = true;
		boolean usersOK = true;
		try {
			jdbcTemplate.execute("SELECT * FROM stylesheets LIMIT 1");
		}
		catch (Exception e) {
			stylesheetsOK = false;
			log.error("Table 'stylesheets' not found!");
		}
		try {
			jdbcTemplate.execute("SELECT * FROM users LIMIT 1");
		}
		catch (Exception e) {
			usersOK = false;
			log.error("Table 'users' not found!");
		}

		// Create missing tables
		if (!stylesheetsOK) {
			log.info("Creating table 'stylesheets'");
			try {
				String sql = "CREATE TABLE IF NOT EXISTS `stylesheets` ("
						+ "`styleID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
						+ "`name` TEXT NOT NULL DEFAULT 'Unnamed',"
						+ "`type` TEXT NOT NULL, "
						+ "`expression` TEXT NOT NULL DEFAULT '', "
						+ "`fg_color` TEXT NOT NULL DEFAULT '#000000',"
						+ "`bg_color` TEXT NOT NULL DEFAULT '#ffffff',"
						+ "`notify` INTEGER NOT NULL DEFAULT 0,"
						+ "`mail` INTEGER NOT NULL DEFAULT 0"
						+ ");";
				jdbcTemplate.execute(sql);
			}
			catch (Exception e) {
				stylesheetsOK = false;
				log.error("Could not create table 'stylesheets'!", e);
			}
		}

		if (!usersOK) {
			log.info("Creating table 'users'");
			try {
				String sql = "CREATE TABLE IF NOT EXISTS `users` ("
						+ "`userID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
						+ "`username` TEXT NOT NULL DEFAULT 'user',"
						+ "`password` TEXT NOT NULL DEFAULT 'pass',"
						+ "`userrole` TEXT NOT NULL DEFAULT 'ROLE_USER'"
						+ ");";
				jdbcTemplate.execute(sql);
			}
			catch (Exception e) {
				stylesheetsOK = false;
				log.error("Could not create table 'users'!", e);
			}
		}
	}


	@Nullable
	public LoginAuthenticationProvider.User getUser(@NonNull String name) {
		checkDatabase();
		LoginAuthenticationProvider.User user;
		String sql = "SELECT `userID`, `username`, `password`, `userrole` FROM `users` WHERE `username` = '" + name + "';";
		try {
			user = jdbcTemplate.queryForObject(sql,
					(rs, i) -> new LoginAuthenticationProvider.User(
							rs.getString("username"),
							rs.getString("password"),
							rs.getString("userrole")
					));
		}
		catch (EmptyResultDataAccessException erdae) {
			log.error("Username not in database!");
			return null;
		}
		return user;
	}

	@NonNull
	public String saveNewCSS(@NonNull JSONObject cssData) {
		checkDatabase();

		String err = "";
		try {
			jdbcTemplate.update(connection -> {
				PreparedStatement stmnt = connection.prepareStatement(
						"INSERT INTO `stylesheets`(`name`,`type`,`expression`,`fg_color`, `bg_color`,`notify`,`mail`) VALUES (?, ?, ?, ?, ?, ?, ?);");
				stmnt.setString(1, cssData.get("name").toString());
				stmnt.setString(2, cssData.get("type").toString());
				stmnt.setString(3, cssData.get("expression").toString());
				stmnt.setString(4, cssData.get("fgcolor").toString());
				stmnt.setString(5, cssData.get("bgcolor").toString());
				stmnt.setInt(6, Integer.parseInt(cssData.get("notify").toString()));
				stmnt.setInt(7, Integer.parseInt(cssData.get("mail").toString()));
				return stmnt;
			});
		}
		catch (Exception e) {
			err = "Error saving style to database!";
			log.error(err, e);
		}
		return err;
	}


	@Nullable
	public ViewStyle loadViewStyle(long styleID) {
		checkDatabase();

		List<ViewStyle> viewStyles = new ArrayList<>();
		try {
			viewStyles = jdbcTemplate.query(
					connection -> {
						PreparedStatement stmnt = connection.prepareStatement(
								"SELECT `styleID`, `name`, `type`, `expression`, `fg_color`, `bg_color`, `notify`, `mail` FROM `stylesheets` WHERE "
										+ "`styleID` = ?");
						stmnt.setLong(1, styleID);
						return stmnt;
					}, (rs, rowNum) -> createViewStyle(rs)
			);
		}
		catch (Exception e) {
			log.error("Failed to load style!", e);
		}

		return !viewStyles.isEmpty() ? viewStyles.get(0) : null;
	}


	@NonNull
	private ViewStyle createViewStyle(@NonNull ResultSet rs) throws SQLException {
		return new ViewStyle(
				rs.getLong("styleID"),
				rs.getString("name"),
				rs.getString("type"),
				rs.getString("expression"),
				rs.getString("bg_color"),
				rs.getString("fg_color"),
				rs.getInt("notify"),
				rs.getInt("mail"));
	}


	@NonNull
	public List<ViewStyle> loadAllStyles() {
		checkDatabase();

		List<ViewStyle> viewStyles = new ArrayList<>();

		try {
			viewStyles = jdbcTemplate.query(connection -> connection.prepareStatement(
					"SELECT `styleID`, `name`, `type`, `expression`, `fg_color`, `bg_color`, `notify`, `mail` FROM `stylesheets`"),
					(rs, rowNum) -> createViewStyle(rs));
		}
		catch (Exception e) {
			log.error("Failed to load styles!", e);
		}
		return viewStyles;
	}


	@NonNull
	public String deleteStylesInRange(@NonNull JSONObject jsonObject) {
		checkDatabase();

		String err = "";
		try {
			jdbcTemplate.update(connection -> {
				PreparedStatement stmnt = connection.prepareStatement("DELETE FROM `stylesheets` WHERE `styleID` IN (?)");
				stmnt.setString(1, jsonObject.get("idlist").toString());
				return stmnt;
					}
			);
		}
		catch (Exception e) {
			err = "Error deleting styles from database!";
			log.error(err, e);
		}
		return err;
	}


	@NonNull
	public String addUser(@NonNull JSONObject jsonObject) {
		checkDatabase();

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String err = "";

		boolean userExists = false;
		try {
			Map<String, Object> found = jdbcTemplate.queryForMap("SELECT 'username' FROM `users` WHERE `username` = ?", jsonObject.get("username"));
			if (!found.isEmpty()) {
				userExists = true;
			}
		}
		catch (EmptyResultDataAccessException ere) {
			log.info("Given name not in use.");
		}

		if (userExists) {
			return "This name is in use!";
		}

		try {
			jdbcTemplate.update(connection -> {
				PreparedStatement stmnt = connection.prepareStatement(
						"INSERT INTO `users`(`username`,`password`, `userrole`) VALUES (?, ?, ?)");
				stmnt.setString(1, jsonObject.get("username").toString());
				stmnt.setString(2, encoder.encode(jsonObject.get("password").toString()));
				stmnt.setString(3, jsonObject.get("userrole").toString());
				return stmnt;
			});
		}
		catch (Exception e) {
			err = "Error adding user into database!";
			log.error(err, e);
		}

		return err;
	}
}
