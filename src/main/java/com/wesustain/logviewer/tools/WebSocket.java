/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.wesustain.logviewer.config.Preferences;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
@Component
public class WebSocket extends TextWebSocketHandler {

	private ConcurrentHashMap<String, LogReader> logReaderMap = new ConcurrentHashMap<>();

	@Autowired
	private DatabaseManager dbm;


	@Override
	public void handleTextMessage(@NonNull WebSocketSession session, @NonNull TextMessage message) {
		//log.info("Message from session " + session + "\n\n" + message.getPayload());
		JSONParser jsonParser = new JSONParser();
		try {
			JSONObject jsonObject = (JSONObject)jsonParser.parse(message.getPayload());
			if (jsonObject.get("action") == null || jsonObject.get("data") == null) {
				return;
			}

			JSONArray dataArray = (JSONArray)jsonObject.get("data");

			if (jsonObject.get("action").equals("startlogreader")) {
				startLogReader(session, (JSONObject)dataArray.get(0));
			}
			else if (jsonObject.get("action").equals("savenewcss")) {
				String ret = dbm.saveNewCSS((JSONObject)dataArray.get(0));
				sendToPage(session, "{\"action\":\"response\", \"data\":[{\"text\":\"" + ret + "\"}]}");
			}
			else if (jsonObject.get("action").equals("getallstyles")) {
				sendToPage(session, getAllStyles());
			}
			else if (jsonObject.get("action").equals("savedeletestyles")) {
				String ret = dbm.deleteStylesInRange((JSONObject)dataArray.get(0));
				sendToPage(session, "{\"action\":\"response\", \"data\":[{\"text\":\"" + ret + "\"}]}");
			}
			else if (jsonObject.get("action").equals("adduser")) {
				String ret = dbm.addUser((JSONObject)dataArray.get(0));
				sendToPage(session, "{\"action\":\"response\", \"data\":[{\"text\":\"" + ret + "\"}]}");
			}
			else if (jsonObject.get("action").equals("dosearch")) {
				if (logReaderMap.containsKey(session.getId())) {
					logReaderMap.get(session.getId()).search((JSONObject)dataArray.get(0));
				}
				else {
					log.error("Logreader instance does not exist!");
				}
			}
			else if (jsonObject.get("action").equals("noop")) {
				// No operation here, only for refresh the HTTP connection
			}
			else {
				log.error("Received data are damaged!");
			}

		}
		catch (ParseException e) {
			e.printStackTrace();
		}

	}


	@Override
	public void afterConnectionEstablished(@NonNull WebSocketSession session) {

		log.info("Connected with session " + session);
	}


	@Override
	public void handleTransportError(@NonNull WebSocketSession session, @NonNull Throwable exception) {
		log.info("WebSocket error: " + exception.toString());
	}


	@Override
	public void afterConnectionClosed(@NonNull WebSocketSession session, @NonNull CloseStatus status) {
		if (logReaderMap.containsKey(session.getId())) {
			logReaderMap.get(session.getId()).stopRunning();
		}
		log.info("Connection closed" + session.toString() + "###" + status);
	}


	private void startLogReader(@NonNull WebSocketSession session, @NonNull JSONObject dataObject) {
		if (logReaderMap.containsKey(session.getId())) {
			logReaderMap.get(session.getId()).stopRunning(); // stop the old one
			logReaderMap.remove(session.getId());
		}
		// Check logFileName to prevent access to other directories than logfiles.path.
		if (!FileUtil.pathIsRight(Preferences.getPreference("logfiles.path"), dataObject.get("logFileName").toString())) {
			log.error("Tried to access a wrong directory!");
			return;
		}
		LogReader logReader = new LogReader(session, dbm, dataObject.get("logFileName").toString(), dataObject.get("lineStyleID").toString(),
				dataObject.get("sectStyleID").toString());
		logReader.start();
		logReaderMap.put(session.getId(), logReader);
	}


	@NonNull
	private String getAllStyles() {
		List<ViewStyle> viewStyles = dbm.loadAllStyles();
		StringBuilder json = new StringBuilder();
		json.append("{\"action\":\"putallstyles\",\"data\":[");
		for (int i = 0; i < viewStyles.size(); i++) {
			json.append("{");
			json.append("\"styleID\":\"").append(viewStyles.get(i).getStyleID()).append("\",");
			json.append("\"name\":\"").append(viewStyles.get(i).getName()).append("\",");
			String expr = StringEscapeUtils.escapeJson(viewStyles.get(i).getExpression());
			json.append("\"expression\":\"").append(expr).append("\",");
			json.append("\"fg_color\":\"").append(viewStyles.get(i).getFgColor()).append("\",");
			json.append("\"bg_color\":\"").append(viewStyles.get(i).getBgColor()).append("\"");
			json.append(viewStyles.size() - 1 == i ? "}" : "},");
		}
		json.append("]}");
		return json.toString();
	}


	public static void sendToPage(@NonNull WebSocketSession session, @NonNull String sendstring) {
		if (session != null && session.isOpen()) {
			try {
				session.sendMessage(new TextMessage(sendstring));
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}