/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.web.socket.WebSocketSession;

import com.wesustain.logviewer.config.Preferences;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
public class LogReader extends Thread {

	private WebSocketSession session;
	private String logFilePath = "";
	private boolean isRunning = false;
	private long contentLength;
	private ViewStyle lineStyle;
	private ViewStyle sectStyle;
	private int lineMatches = 0; // count line-matches for display in notification
	private int sectMatches = 0; // count section-matches for display in notification
	private StringBuilder mailTextLine = new StringBuilder(); // collect bodycontent for sending via mail
	private StringBuilder mailTextSect = new StringBuilder(); // collect bodycontent for sending via mail


	public LogReader(@NonNull WebSocketSession session, @NonNull DatabaseManager dbm, @NonNull String logFileName, @NonNull String lineStyleID,
					 @NonNull String sectStyleID) {
		this.session = session;
		if (!lineStyleID.isEmpty()) {
			lineStyle = dbm.loadViewStyle(Long.parseLong(lineStyleID));
		}
		if (!sectStyleID.isEmpty()) {
			sectStyle = dbm.loadViewStyle(Long.parseLong(sectStyleID));
		}
		this.logFilePath = Preferences.getPreference("logfiles.path") + logFileName;
	}


	@Override
	public void run() {
		isRunning = true;
		while (isRunning) {
			readLog();
			try {
				sleep(Long.parseLong(Preferences.getPreference("readlog.interval")));
			}
			catch (InterruptedException e) {
				log.error("Failed to start running", e);
			}
		}
		log.info("Close LogReader with Session " + session.getId());
		isRunning = false;
	}


	public void stopRunning() {
		isRunning = false;
	}


	private void readLog() {
		lineMatches = 0;
		sectMatches = 0;
		mailTextLine.setLength(0);
		mailTextSect.setLength(0);
		RandomAccessFile logFile;
		try {
			logFile = new RandomAccessFile(logFilePath, "r");
			if (contentLength - 1 > logFile.length()) {
				contentLength = 0L;
				WebSocket.sendToPage(session, "{\"action\":\"clearall\", \"data\":[{\"\":\"\"}]}");
			}

			StringBuilder lines = new StringBuilder();
			String line = "";
			long read = 0L;
			while ((line = logFile.readLine()) != null) {
				read += line.length() + 1; // +1 for '\n' lost in method readLine
				if (read > contentLength) {
					if (sectStyle != null) {
						line = applySectStyle(line);
					}

					lines.append(applyLineStyle(line));
					lines.append("SEPARATOR");

					contentLength = read;
				}
			}

			logFile.close();

			if (lines.length() != 0) {
				WebSocket.sendToPage(session, lines.toString());

				// Notification
				String bodyText = "";
				if (lineStyle != null && lineStyle.getNotification() == 1 && lineMatches > 0) {
					bodyText += prepareTextForLineMatches() + JSONValue.escape("\n\n");
				}
				if (sectStyle != null && sectStyle.getNotification() == 1 && sectMatches > 0) {
					bodyText += prepareTextForSecMatches();
				}
				if (!bodyText.isEmpty()) {
					WebSocket.sendToPage(session, "{\"action\":\"notify\", \"data\":[ {\"bodyText\": \"" + bodyText + "\"}]}");
				}

				// Send email
				bodyText = "";
				if (lineStyle != null && lineStyle.getMail() == 1) {
					bodyText += "<b>" + prepareTextForLineMatches() + "</b><br>" + mailTextLine;
				}
				if (sectStyle != null && sectStyle.getMail() == 1) {
					bodyText += "<b>" + prepareTextForSecMatches() + "</b><br>" + mailTextSect;
				}
				if (!bodyText.isEmpty()) {
					sendMail(bodyText);
				}
			}
		}
		catch (IOException e) {
			log.error(e.getMessage());
		}
	}


	@NonNull
	private String applySectStyle(@NonNull String line) {
		boolean found = false;
		Pattern pattern = Pattern.compile(sectStyle.getExpression(), Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(line);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "<span class=\"" + sectStyle.getName() + "\">" + matcher.group() + "</span>");
			sectMatches++;
			found = true;
		}
		matcher.appendTail(sb);
		if (found && sectStyle.getMail() == 1) {
			mailTextSect.append(line).append("<br>");
		}
		return sb.toString();
	}


	@NonNull
	private String applyLineStyle(@NonNull String line) {
		String styleClass = "normal";
		if (lineStyle != null && !lineStyle.getExpression().isEmpty()) {
			Pattern pattern = Pattern.compile(lineStyle.getExpression(), Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(line);
			if (matcher.find()) {
				styleClass = lineStyle.getName();
				lineMatches++;
				if (lineStyle.getMail() == 1) {
					mailTextLine.append(line).append("<br>\n");
				}
			}
		}
		return "<p class=\"" + styleClass + "\">" + line + "</p>";
	}


	@NonNull
	private String prepareTextForLineMatches() {
		StringBuilder bodyText = new StringBuilder();
		if (lineMatches > 0) {
			bodyText.append(lineMatches == 1 ? "One new entry " : lineMatches + " new entries ");
			bodyText.append("matching the expression '").append(JSONValue.escape(lineStyle.getExpression())).append("' found in the logfile!");
		}
		return bodyText.toString();
	}


	@NonNull
	private String prepareTextForSecMatches() {
		StringBuilder bodyText = new StringBuilder();
		if (sectMatches > 0) {
			bodyText.append(sectMatches == 1 ? "One new entry " : sectMatches + " new entries ");
			bodyText.append("matching the expression '").append(JSONValue.escape(sectStyle.getExpression())).append("' found in the logfile!");
		}
		return bodyText.toString();
	}


	private void sendMail(@NonNull String messageText) {
		javax.mail.Session mailSession = javax.mail.Session.getInstance(Preferences.getMailPreferences(),
				new javax.mail.Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(Preferences.getPreference("mail.username"), Preferences.getPreference("mail.password"));
					}
				});
		try {
			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(Preferences.getPreference("mail.sender.address")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Preferences.getPreference("mail.recipient.address")));
			message.setSubject("Information about occurences from LogViewer");
			message.setContent(messageText, "text/html");
			Transport.send(message);
			log.info("Info-mail sent!");
		}
		catch (MessagingException e) {
			log.error("Email could not be sent!", e);
		}
	}


	public void search(@NonNull JSONObject jsonObject) {
		RandomAccessFile logFile;
		StringBuilder lines = new StringBuilder();
		try {
			logFile = new RandomAccessFile(logFilePath, "r");
			String line = "";
			while ((line = logFile.readLine()) != null) {
				boolean found = false;
				Pattern pattern = Pattern.compile(jsonObject.get("lookfor").toString(), Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(line);
				StringBuffer sb = new StringBuffer();
				while (matcher.find()) {
					matcher.appendReplacement(sb, "<span class=\"found\">" + matcher.group() + "</span>");
					found = true;
				}
				matcher.appendTail(sb);

				// if(found){
				lines.append("<p class=\"normal\">");
				lines.append(sb.toString());
				lines.append("</p>SEPARATOR");
				//}


			}
			logFile.close();

			if (lines.length() == 0) {
				WebSocket.sendToPage(session, "<p class=\"normal\">Nothing found!</p>");
			}
			else {
				WebSocket.sendToPage(session, lines.toString());
			}


		}
		catch (FileNotFoundException e) {
			log.error(e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
