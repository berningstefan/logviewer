/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import lombok.NonNull;

/**
 * @author sberning
 */
public class FileUtil {

	@NonNull
	public static boolean pathIsRight(@NonNull String dirName, @NonNull String fileName) {
		Path dirNamePath = FileSystems.getDefault().getPath(dirName, "");
		Path fileNamePath = FileSystems.getDefault().getPath("", fileName);
		Path resolvedPath = dirNamePath.resolve(fileNamePath).normalize();

		return resolvedPath.startsWith(dirNamePath);
	}
}
