/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
@Component
public class LoginAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private DatabaseManager dbm;


	@NonNull
	@Override
	public Authentication authenticate(@NonNull Authentication authentication) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String name = authentication.getName();
		User user = dbm.getUser(name);
		if (user == null) {
			throw new BadCredentialsException("Authentication failed!");
		}
		Optional<User> userOptional = Optional.empty();

		Object credentials = authentication.getCredentials();
		String password = credentials.toString();

		if (user != null && user.NAME.equals(name) && encoder.matches(password, user.PASSWORD)) {
			userOptional = Optional.of(user);
		}
		else {
			throw new BadCredentialsException("Authentication failed!");
		}

		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(userOptional.get().ROLE));
		return new UsernamePasswordAuthenticationToken(name, password, grantedAuthorities);
	}


	@NonNull
	@Override
	public boolean supports(@NonNull Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}


	@AllArgsConstructor
	public static class User {
		private final String NAME;
		private final String PASSWORD;
		private final String ROLE;


		@NonNull
		public boolean match(@NonNull String name, @NonNull String password) {
			return this.NAME.equals(name) && this.PASSWORD.equals(password);
		}
	}
}
