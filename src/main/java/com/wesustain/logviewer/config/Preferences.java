/*
 *  LogViewer - A small tool to observe your logfiles.
 *  Copyright (C) 2018/2019 Stefan Berning
 *  Copyright (C) 2018/2019 WeSustain GmbH
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.wesustain.logviewer.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sberning
 */
@Slf4j
public class Preferences {
	private static Properties properties = new Properties();


	public Preferences() {
		loadPreferences();
	}


	private void loadPreferences() {
		try {
			properties.loadFromXML(new FileInputStream(System.getProperty("resources.dir") + "preferences.xml"));
		}
		catch (IOException e) {
			log.error("Failed to load preferences", e);
			throw new RuntimeException(e);
		}
	}


	@NonNull
	public static String getPreference(@NonNull String key) {
		String val = properties.getProperty(key);
		if (val != null) {
			return val;
		}
		else {
			return "";
		}
	}


	@NonNull
	public static Properties getMailPreferences() {
		Properties mailProperties = new Properties();
		mailProperties.setProperty("mail.smtp.auth", getPreference("mail.smtp.auth"));
		mailProperties.setProperty("mail.smtp.starttls.enable", getPreference("mail.smtp.starttls.enable"));
		mailProperties.setProperty("mail.smtp.ssl.trust", getPreference("mail.smtp.ssl.trust"));
		mailProperties.setProperty("mail.smtp.host", getPreference("mail.smtp.host"));
		mailProperties.setProperty("mail.smtp.port", getPreference("mail.smtp.port"));
		return mailProperties;
	}
}
